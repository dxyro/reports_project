from email import message

from django.core.mail import EmailMessage

from reports_app.utils.export_process import XLSXCreator
from reports_project.celery import app
from reports_project.local_settings import EMAIL_HOST_USER
import random as ra
import os

from reports_project.settings import BASE_DIR


@app.task()
def export_file(information):
    file = XLSXCreator('report_{}.xlsx'.format(ra.randint(1, 30)))
    file_parser = file.filter(**information)
    file.field(customer='customer', email_id='email id', option='option', filename='filename',
               email_date='email date', create_ad='created at')
    file.value_field(file_parser)
    sendmail.delay(file.name)
    return 'success'


@app.task()
def sendmail(path_file):
    message = 'report has been create successfully'
    msg = EmailMessage(
            'Report info',
            message,
            EMAIL_HOST_USER,
            ['dxyro_05@hotmail.com'],
    )
    msg.content_subtype = "html"
    try:
        msg.attach_file(f'{BASE_DIR}/{path_file}')
    except Exception as e:
        msg.body = 'The file could not be sent because no records were found.'
        print(e)

    msg.send()

    return 'Email sent'
