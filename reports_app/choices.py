class DEFAULTS:
    PARSER_CHOICE = 'A'
    OPTION_UPLOAD = 'upload_by_web'


parser_choice = (
    ('A', 'parserA'),
    ('B', 'parserB'),
    ('C', 'parserC'),
)

option_upload = (
    ('by_email', 'Email'),
    ('upload_by_web', 'Web'),
)

filter_type = (
        ('email', 'Email'),
        ('date', 'Date'),
)
