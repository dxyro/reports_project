from openpyxl import Workbook
from reports_app.models import FileParser


class XLSXCreator():
    def __init__(self, name):
        self.name = name
        self.wb = Workbook()
        self.ws = self.wb.active

    def field(self, *args, **kwargs):
        list_value = []
        for key, value in kwargs.items():
            list_value.append(key)
        self.ws.append(list_value)

    def value_field(self, objects):
        for obj in objects:
            name = obj.customer
            email = obj.email_id
            email_date = obj.email_date
            option = obj.option
            filename = obj.file_name
            filename = ''.join(str(filename).split('/')[1:])
            create = obj.created_at
            self.ws.append([str(name), str(email), str(option), str(filename), str(email_date), str(create)])
            self.wb.save(str(self.name))
        return None

    def filter(self, *args, **kwargs):

        file_parser = ''
        if kwargs['filter_by'] == 'email':
            # customer email
            file_parser = FileParser.objects.filter(customer=kwargs['customer_id'],
                                                    email_date__gte=kwargs['initial_date'],
                                                    email_date__lte=kwargs['finish_date'],
                                                    sent=kwargs['status'])

        if kwargs['filter_by'] == 'date':
            # customer created_at
            file_parser = FileParser.objects.filter(customer=kwargs['customer_id'],
                                                    created_at__gte=kwargs['initial_date'],
                                                    created_at__lte=kwargs['finish_date'],
                                                    sent=kwargs['status'])
        return file_parser
