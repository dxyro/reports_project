from django.contrib import admin

from reports_app.models import Customer, FileParser

admin.site.register(Customer)
admin.site.register(FileParser)
