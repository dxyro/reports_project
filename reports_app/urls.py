from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='index'),
    path('create_files/', views.CreateFileParser.as_view(), name='create_file'),
    path('filter/', views.FilterView.as_view(), name='filter'),
    path('create_custom/', views.CreateCustom.as_view(), name='create_custom'),
]
