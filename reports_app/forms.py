import datetime

from django import forms

from reports_app.choices import filter_type
from reports_app.models import Customer, FileParser


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer

        fields = [
            'name',
            'parser',
        ]

        labels = {
            'name': 'Nombre',
            'parser': 'Parser',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'parser': forms.Select(attrs={'class': 'form-control'}),
        }


class FileParserForm(forms.ModelForm):
    class Meta:
        model = FileParser
        fields = [
            'customer',
            'email_id',
            'email_date',
            'option',
            'sent',
            'file_name',
        ]

        labels = {
            'customer': 'Customer',
            'email_id': 'Email id',
            'email_date': 'Email Date',
            'option': 'Option',
            'sent': 'Sent',
            'file_name': 'File',
        }

        widgets = {
            'customer': forms.Select(attrs={'class': 'form-control'}),
            'email_id': forms.TextInput(attrs={'class': 'form-control'}),
            'email_date': forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'date'}),
            'option': forms.Select(attrs={'class': 'form-control'}),
            'sent': forms.CheckboxInput(attrs={'type': 'checkbox'}),
            'file_name': forms.ClearableFileInput(attrs={'class': 'file-field', 'multiple': True, 'type': 'file'})
        }


class FilterForm(forms.Form):
    customer = forms.ModelChoiceField(queryset=Customer.objects.all(),
                                      widget=forms.Select(attrs={'class': 'form-control'}))
    filter_by = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
                                  choices=filter_type)
    initial_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    finish_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    status = forms.NullBooleanField(required=False,
                                    widget=forms.CheckboxInput(attrs={'type': 'checkbox'}))

    def clean(self):
        cleaned_data = super(FilterForm, self).clean()
        initial_date = cleaned_data.get('initial_date')
        finish_date = cleaned_data.get('finish_date')
        if finish_date <= datetime.date.today():
            if initial_date > finish_date:
                raise forms.ValidationError("The start date cannot be greater than the end date.")
        else:
            raise forms.ValidationError("The end date cannot be greater than today.")
        return cleaned_data
