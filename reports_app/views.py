from django.http import HttpResponse
from django.views.generic import CreateView, FormView
from reports_app.models import Customer
from reports_app.forms import CustomerForm, FileParserForm, FilterForm
from django.views import View
from django.shortcuts import render
from django.urls import reverse_lazy
from .utils.export_process import *
from .tasks import export_file, sendmail
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "reports_app/index.html"


class CreateCustom(CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'reports_app/register.html'
    success_url = reverse_lazy('create_custom')


class CreateFileParser(CreateView):
    model = FileParser
    form_class = FileParserForm
    template_name = 'reports_app/register_file.html'
    success_url = reverse_lazy('create_file')


class FilterView(FormView):
    form_class = FilterForm
    template_name = 'reports_app/filter.html'
    success_url = reverse_lazy('index')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            try:
                bool = request.POST['status']
                status_bool = True
            except:
                status_bool = False

            info_request = {'filter_by': request.POST['filter_by'], 'customer_id': request.POST['customer'],
                            'initial_date': request.POST['initial_date'], 'finish_date': request.POST['finish_date'],
                            'status': status_bool}
            export_file.delay(info_request)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
