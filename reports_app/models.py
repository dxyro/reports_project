from django.db import models

from reports_app.choices import parser_choice, option_upload, DEFAULTS


class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    parser = models.CharField(max_length=1, choices=parser_choice, default=DEFAULTS.PARSER_CHOICE)

    def __str__(self):
        return self.name


class FileParser(models.Model):
    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email_id = models.IntegerField(default=0)
    email_date = models.DateField()
    option = models.CharField(max_length=30, choices=option_upload, default=DEFAULTS.OPTION_UPLOAD)
    sent = models.BooleanField(default=False, blank=True, null=True)
    file_name = models.FileField(upload_to='uploads/')
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
       return self.customer.name

    def get_info(self):
        return 'id {} , customer {} , email_id {} , email_date {} ,  option {}, sent {}' \
               ''.format(self.id, self.customer, self.email_id, self.email_date, self.option, self.sent)


